#!/bin/sh

self=$(readlink -mn $(dirname "$BASH_SOURCE"))
modulebase=$(readlink -mn $(dirname "$self"))

cp $self/template.path $self/docker-mm.path
cp $self/template.service $self/docker-mm.service

sed -i 's|xxxxxxxxxx|'$modulebase'|g' $self/docker-mm.path
sed -i 's|xxxxxxxxxx|'$modulebase'|g' $self/docker-mm.service

sudo mv -vf $self/docker-mm.* /usr/lib/systemd/system/

sudo systemctl daemon-reload

sudo systemctl enable docker-mm.path
sudo systemctl enable docker-mm.service

sudo systemctl start docker-mm.path

sudo systemctl status docker-mm.path
sudo systemctl status docker-mm.service
