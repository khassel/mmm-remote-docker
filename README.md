**MagicMirror Module to shutdown/reboot your raspberry pi running the MagicMirror as docker container**

MagicMirror² is an open source modular smart mirror platform. For more info visit the [project website](https://github.com/MichMich/MagicMirror).

# Why a new module?

Running on debian stretch it was quite simple to shutdown the host from inside a running container. You only needed to mount `/run/systemd:/run/systemd` and
to install `systemd-sysv` in the container, this was sufficient to execute the `shutdown` command in the container for shutdown the host.

With debian buster this is now much more complicated. So I decided to write a "clean" solution.

# Use case

This module is only helpful if your MagicMirror is running as a docker container on your raspberry pi.
If you installed your MagicMirror directly on your raspberry pi (without docker), you can use
the [MMM-Remote-Control Module](https://github.com/Jopyth/MMM-Remote-Control) for shutting down your host.

# 2 Parts

### Part #1: Service on the host

On the host you have to install a service which listen on changes of a specific file. 
If this file is written, the content of this file will be executed by this service.

### Part #2: Trigger Shutdown in the container

From inside the container you trigger the creation of the mentioned file. This can be done by executing a specific url in
your browser or by using a custom menu of the [MMM-Remote-Control Module](https://github.com/Jopyth/MMM-Remote-Control).

# Installation

### Install the module in your MagicMirror

- Start the docker container an log in the container with `docker exec -it mm bash`, where mm is the name of the container.

- Navigate into your `modules` folder and clone this repository and install dependencies:
```bash
cd ./modules # adapt directory if you are using a different one
git clone https://gitlab.com/khassel/mmm-remote-docker
cd mmm-remote-docker
npm install
```

- Shutdown the docker container.

- Navigate to the directory on the host, where the `config` directory is located, e.g. `/home/pi/magicmirror/mounts/config`.
  Add the module to your `config.js` file.
```js
{
  module: 'mmm-remote-docker',
  config: {
    shutdowntime: 1 // optional, the time in minutes before shutting down. Default=1, 0=immediate shutdown.
  }
},
```

- Start the docker container.

### Update

- Start the docker container an log in the container with `docker exec -it mm bash`, where mm is the name of the container.

- Navigate into your `modules` folder and execute:
```bash
cd ./modules/mmm-remote-docker # adapt directory if you are using a different one
git pull
npm install
```

### Install the service on the host

On the host navigate to the folder where you installed this module, e.g. `/home/pi/magicmirror/mounts/modules/mmm-remote-docker`.

```bash
cd ./service
./install_service.sh
```

> For removing the service you can use `./uninstall_service.sh` in the same directory.

# Execute Shutdown (or Reboot)

### Shutdown using url's

Use the following url's to trigger a shutdown or reboot where you have to replace `192.168.xxx.xxx` with the IP address of your raspberry pi:

Shutdown: http://192.168.xxx.xxx:8080/docker?action=SHUTDOWN
  
Reboot: http://192.168.xxx.xxx:8080/docker?action=REBOOT

> You can cancel the shutdown by executing `sudo shutdown -c` on the host (if you set `shutdowntime` > 0 and you are fast enough ...)

### Shutdown using the MMM-Remote-Control Module

You can configure a custom menu in the MMM-Remote-Control Module. First you have to install the [MMM-Remote-Control Module](https://github.com/Jopyth/MMM-Remote-Control).
Then you create a custom menu following [this description](https://github.com/Jopyth/MMM-Remote-Control#custom-menu-items).

As custom menu you have to use the file `custom_menu.json` which is located in the `config` directory of this repository.
Copy this file into the `config` directory of the MagicMirror (e.g. `/home/pi/magicmirror/mounts/config`) and create the 
`customMenu: "custom_menu.json"` entry in the `config.js`.

After restarting the docker container you find a new submenu `Docker` executing this url http://192.168.xxx.xxx:8080/remote.html for shutdown and reboot.


