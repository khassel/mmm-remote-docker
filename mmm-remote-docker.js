Module.register("mmm-remote-docker", {
  requiresVersion: "2.4.0",

  // Default module config.
  defaults: {
    // 1 minute, 0=now
    shutdowntime: 1,
  },

  // Define start sequence.
  start: function () {
    this.sendSocketNotification("CONFIG", this.config);
  },

  notificationReceived: function (notification, payload, sender) {
    if (notification == "NOTIFICATION") {
      this.sendSocketNotification("NOTIFICATION", payload);
    }
  },
});
