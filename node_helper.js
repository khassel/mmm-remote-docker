// usage: http://192.168.0.143:8080/docker?action=REBOOT
//        http://192.168.0.143:8080/docker?action=SHUTDOWN

const NodeHelper = require("node_helper");
const url = require("url");
const fs = require("fs");
const path = require("path");

module.exports = NodeHelper.create(
  Object.assign({
    // Subclass start method.
    start: () => {
      var self = this;

      this.initialized = false;
      console.log("Starting node helper for: " + self.name);

      this.createRoutes();
    },

    createRoutes: () => {
      var self = this;

      this.expressApp.get("/docker", (req, res) => {
        var query = url.parse(req.url, true).query;
        // console.dir(query, { depth: null });
        if (query.action) {
          var result = self.executeQuery(query, res);
          console.log("executeQuery " + query.action + " result=" + result);
          if (result === true) {
            return;
          }
        }
        res.send({
          status: "error",
          reason: "unknown_command",
          info: "original input: " + JSON.stringify(query),
        });
      });
    },

    executeQuery: (query, res) => {
      var self = this;
      var opts = { timeout: 15000 };
      var command = "";

      if (query.action === "SHUTDOWN") {
        command =
          "#!/bin/sh\n\nshutdown -h +" + this.config.shutdowntime + "\n";
      }
      if (query.action === "REBOOT") {
        command =
          "#!/bin/sh\n\nshutdown -r +" + this.config.shutdowntime + "\n";
      }
      if (query.action === "TEST") {
        this.sendSocketNotification("NOTIFICATION", "TEST");
        self.sendResponse(res);
        return true;
      }

      if (command) {
        fs.writeFile(
          path.resolve(__dirname + "/hostshutdown.sh"),
          command,
          (err) => {
            if (err) {
              console.log("Error writing file hostshutdown.sh: " + err);
              self.sendResponse(
                res,
                new Error(`Invalid Option: ${query.action}`)
              );
              return false;
            }
          }
        );

        console.log("The file hostshutdown.sh was saved!");
        self.sendResponse(res);
        return true;
      }
    },

    sendResponse: (res, error, data) => {
      let response = { success: true };
      let status = 200;
      let result = true;
      if (error) {
        console.log(error);
        response = {
          success: false,
          status: "error",
          reason: "unknown",
          info: error,
        };
        status = 400;
        result = false;
      }
      if (data) {
        response = Object.assign({}, response, data);
      }
      if (res) {
        res.status(status).json(response);
      }
      return result;
    },

    checkForExecError: (error, stdout, stderr, res, data) => {
      console.log(stdout);
      console.log(stderr);
      this.sendResponse(res, error, data);
    },

    // Subclass socketNotificationReceived received.
    socketNotificationReceived: (notification, payload) => {
      var self = this;

      if (notification === "CONFIG") {
        this.config = payload;
        // console.dir(payload);
      }
      if (notification === "NOTIFICATION") {
        if (payload === "DOCKER_SHUTDOWN") {
          query = Object.assign({}, "", { action: "SHUTDOWN" });
        }
        if (payload === "DOCKER_REBOOT") {
          query = Object.assign({}, "", { action: "REBOOT" });
        }
        if (query) {
          var result = self.executeQuery(query);
          console.log("executeQuery " + query.action + " result=" + result);
        }
      }
      if (notification === "TEST") {
        console.log("TEST");
        console.dir(payload);
      }
    },
  })
);
