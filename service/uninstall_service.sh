#!/bin/sh

sudo systemctl stop docker-mm.path

sudo systemctl disable docker-mm.path
sudo systemctl disable docker-mm.service

sudo rm -f /usr/lib/systemd/system/docker-mm.*

sudo systemctl daemon-reload


